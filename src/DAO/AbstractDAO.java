package DAO;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * AbstractDAO<T> is a class that implements reflection methods for data manipulation with MySql tables.
 * It is parametric, the parameter T.
 * @param <T>
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	/**
	 * A class of type equal to param. <T>
	 */
	private final Class<T> type;

	/**
	 * Constructor of AbstractDAO that creates an instance of type <T> and assignes it to type
	 */
	public AbstractDAO() {
		this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * A function that creates a query that selects from table "type"  every row,
	 * where the collon "field" = the input
	 * @param field
	 * @return the full query
	 */
	private String createSelectQuery(String field) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT ");
		stringBuilder.append(" * ");
		stringBuilder.append(" FROM ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" WHERE " + field + " = ?");
		return stringBuilder.toString();
	}

	/**
	 * A function that creates a query that selects every row from the table "type"
	 * @return the full Select query
	 */
	private String createFindAllQuery() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT ");
		stringBuilder.append(" * ");
		stringBuilder.append(" FROM ");
		stringBuilder.append(type.getSimpleName());
		return stringBuilder.toString();
	}

	/**
	 * Selects from the table type an object with id = to int id.
	 * If no such object, returns null.
	 * @param id
	 * @return Object of type T
	 */
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Selects from the table "type" an object with collon = value.
	 * If no such object, returns null.
	 * @param collon
	 * @param value
	 * @return Object T
	 */
	public ArrayList<T> findByVariable(String collon, String value) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(collon);

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, value);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findByVariable " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * This function has as input an Arraylist of Object type.
	 * For each Object from the ArrayList it calls the insertIntoTable function.
	 * @param objs
	 */
	public void createTable(ArrayList<Object> objs) {
		for (Object obj: objs) {
			insertIntoTable(obj);
		}
	}

	/**
	 * This function creates an Insert query that puts the Object obj into the table
	 * with the name equal to type.
	 * @param obj
	 */
	public void insertIntoTable(Object obj) {
		Connection connection = null;
		PreparedStatement statement = null;

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("INSERT INTO ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" (");

		for (Field field : obj.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			if (!field.getName().equals("id")) {
				try {
					stringBuilder.append(" " + field.getName() + ",");
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			}
		}

		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		stringBuilder.append(") VALUES (");

		for (Field field : obj.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			if (!field.getName().equals("id")) {
				try {
					value = field.get(obj);
					stringBuilder.append(" '" + value + "',");
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		stringBuilder.deleteCharAt(stringBuilder.length() - 1 );
		stringBuilder.append(")");

		System.out.println(stringBuilder.toString());

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(stringBuilder.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insertInto " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}

	/**
	 * This function finds all the objects from a table with the name equal to type;
	 * @return ArrayList of type T with objects from table, if there are no objects
	 * in the tablem the ArrayList will be null
	 */
	public ArrayList<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createFindAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * This function creates an ArrayList of type T from an resulting Set generated by a query.
	 * @param resultSet
	 * @return ArrayList of type T
	 */
	private ArrayList<T> createObjects(ResultSet resultSet) {
		ArrayList<T> list = new ArrayList<T>();
		
		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field: type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}


	/**
	 * Updates an table with name = type T.
	 * @param t an object that contains the information about the row that should be updated. If the information about
	 *         	the object is incorrect, it will fire an SQLException, without alternations on the table.
	 * @return the object t
	 */
	public T update(T t) {
		Connection connection = null;
		PreparedStatement statement = null;

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Update ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" SET ");

		for (Field field : t.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			if (!field.getName().equals("id")) {
				try {
					stringBuilder.append("" + field.getName() + " = '");
					stringBuilder.append(field.get(t));
					stringBuilder.append("',");
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		stringBuilder.deleteCharAt(stringBuilder.length() - 1 );

		Field field  = null;
		try {
			field = t.getClass().getDeclaredField("id");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		stringBuilder.append(" WHERE id = ");
		try {
			stringBuilder.append(field.get(t));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		System.out.println(stringBuilder.toString());

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(stringBuilder.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insertInto " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

		return t;
	}

	/**
	 * This function deletes from the table <i>type</i> the row which has the id = to int id
	 * @param id
	 */
	public void delete(int id) {
		Connection connection = null;
		PreparedStatement statement = null;

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("DELETE FROM ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" WHERE id = ");
		stringBuilder.append(id);
		System.out.println(stringBuilder.toString());

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(stringBuilder.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insertInto " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
}
