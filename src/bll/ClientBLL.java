package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import DAO.AbstractDAO;
import DAO.ClientDAO;
import model.Client;

/**
 * The layer of the application that is the link between the UI and the layer that queries the DataBase
 */
public class ClientBLL {

	private List<Validator<Client>> validators;
	private AbstractDAO<Client> abstractDAO;
	/**
	 * The constructor of the class ClientBLL inits the ArrayList
	 * that contains validators for the information about
	 * clients. It Validates the mail and the phone number.
	 * Also it creates the link with the Data Access Object layer <i>ClientDAO</i>
	 */
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new NumberValidator());

		abstractDAO = new ClientDAO();
	}
	/**
	 * The method that calls the <i>abstractDAO</i> layer to execute the
	 * query that searches for an object with the id = id
	 * @param id
	 * @return Client with the id = id  from the table
	 */
	public Client findClientById(int id) {
		Client st = abstractDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The student with id =" + id + " was not found!");
		}
		return st;
	}
	/**
	 * The method that calls the <i>abstractDAO</i> layer to execute the
	 * query that searches for all the objects from the table.
	 * @return ArrayList of the found objects of Clients
	 */
	public ArrayList<Client> findAll() {
		ArrayList<Client> st = abstractDAO.findAll();

		if (st == null) {
			throw new NoSuchElementException("No data in this table!");
		}

		return st;
	}
	/**
	 * The method that calls the <i>abstractDAO</i> layer to execute the
	 * query that searches for all the objects from the table that have the collon = value
	 * @param collon
	 * @param value
	 * @return ArrayList of the found objects of Clients
	 */
	public ArrayList<Client> findAllBy(String collon, String value) {
		ArrayList<Client> st = abstractDAO.findByVariable(collon, value);

		if (st == null) {
			throw new NoSuchElementException("No data in this table!");
		}

		return st;
	}
	/**
	 * The method that calls the <i>abstractDAO</i> layer to execute the insertion
	 * into the table Client with the object c
	 * @param c
	 */
	public void insert(Client c) {
		abstractDAO.insertIntoTable(c);
	}
	/**
	 * The method that calls the <i>abstractDAO</i> layer to execute the update
	 * on the table Client with the other the object c
	 * @param c
	 */
	public void update(Client c) {
		abstractDAO.update(c);
	}
	/**
	 * The method that calls the <i>abstractDAO</i> layer to execute the delete
	 * on the table Client, it deletes  object c
	 * @param c
	 */
	public void delete(Client c) { abstractDAO.delete(c.getId());}
}
