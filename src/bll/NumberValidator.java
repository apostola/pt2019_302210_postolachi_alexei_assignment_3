package bll;

import java.util.regex.Pattern;

import model.Client;

public class NumberValidator implements Validator<Client>{
	private static final String PHONE_PATTERN = "([0-9])+";
	
	public void validate(Client client) {
		Pattern pattern = Pattern.compile(PHONE_PATTERN);
		if (!pattern.matcher(client.getPhoneNumber()).matches() && client.getPhoneNumber().length() != 10) {
			throw new IllegalArgumentException("Phone number is not valid!");
		}
	}

}
