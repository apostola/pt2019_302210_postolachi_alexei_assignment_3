package bll;

import DAO.AbstractDAO;
import DAO.OrderDAO;
import model.Comenzi;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * The layer of the application that is the link between the UI and the layer that queries the DataBase
 */
public class OrderBLL  {

    private AbstractDAO<Comenzi> abstractDAO;
    /**
     * The constructor of the class OrderBLL creates the link
     * with the Data Access Object layer <i>OrderDAO</i>
     */
    public OrderBLL() {
        abstractDAO = new OrderDAO();
    }
    /**
     * The method that calls the <i>OrderDAO</i> layer to execute the
     * query that searches for an object with the id = id
     * @param id
     * @return Comenzi with the id = id  from the table
     */
    public Comenzi findOrderById(int id) {
        Comenzi st = abstractDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }
    /**
     * The method that calls the <i>OrderDAO</i> layer to execute the
     * query that searches for all the objects from the table.
     * @return ArrayList of the found objects of Comenzi
     */
    public ArrayList<Comenzi> findAll() {
        ArrayList<Comenzi> st = abstractDAO.findAll();

        if (st == null) {
            throw new NoSuchElementException("No data in this table!");
        }

        return st;
    }
    /**
     * The method that calls the <i>OrderDAO</i> layer to execute the
     * query that searches for all the objects from the table that have the collon = value
     * @param collon
     * @param value
     * @return ArrayList of the found objects of Comenzi
     */
    public ArrayList<Comenzi> findOrderBy(String collon, String value) {
        ArrayList<Comenzi> st = abstractDAO.findByVariable(collon, value);

        if (st == null) {
            throw new NoSuchElementException("No data in this table!");
        }

        return st;
    }
    /**
     * The method that calls the <i>OrderDAO</i> layer to execute the insertion
     * into the table Comenzi with the object c
     * @param c
     */
    public void insert(Comenzi c) {
        abstractDAO.insertIntoTable(c);
    }
    /**
     * The method that calls the <i>OrderDAO</i> layer to execute the update
     * on the table Comenzi with the other the object c
     * @param c
     */
    public void update(Comenzi c) {
        abstractDAO.update(c);
    }
    /**
     * The method that calls the <i>OrderDAO</i> layer to execute the delete
     * on the table Comenzi, it deletes  object c
     * @param c
     */
    public void delete(Comenzi c) { abstractDAO.delete(c.getId());}
}
