package bll;

import DAO.ProductDAO;
import  DAO.AbstractDAO;
import model.Product;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * The layer of the application that is the link between the UI and the layer that queries the DataBase
 */
public class ProductBLL{
    private AbstractDAO<Product> abstractDAO;
    /**
     * The constructor of the class AbstractDAO creates the link
     * with the Data Access Object layer <i>ProductDAO</i>
     */
    public ProductBLL() {
        abstractDAO = new ProductDAO();
    }
    /**
     * The method that calls the <i>ProductDAO</i> layer to execute the
     * query that searches for an object with the id = id
     * @param id
     * @return Product with the id = id  from the table
     */
    public Product findProductById(int id) {
        Product st = abstractDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return st;
    }
    /**
     * The method that calls the <i>ProductDAO</i> layer to execute the
     * query that searches for all the objects from the table.
     * @return ArrayList of the found objects of Product
     */
    public ArrayList<Product> findAll() {
        ArrayList<Product> st = abstractDAO.findAll();

        if (st == null) {
            throw new NoSuchElementException("No data in this table!");
        }

        return st;
    }
    /**
     * The method that calls the <i>ProductDAO</i> layer to execute the
     * query that searches for all the objects from the table that have the collon = value
     * @param collon
     * @param value
     * @return ArrayList of the found objects of Product
     */
    public ArrayList<Product> findAllBy(String collon, String value) {
        ArrayList<Product> st = abstractDAO.findByVariable(collon, value);

        if (st == null) {
            throw new NoSuchElementException("No data in this table!");
        }

        return st;
    }
    /**
     * The method that calls the <i>ProductDAO</i> layer to execute the insertion
     * into the table Product with the object c
     * @param c
     */
    public void insert(Product c) {
        abstractDAO.insertIntoTable(c);
    }
    /**
     * The method that calls the <i>ProductDAO</i> layer to execute the update
     * on the table Product with the other the object c
     * @param c
     */
    public void update(Product c) {
        abstractDAO.update(c);
    }
    /**
     * The method that calls the <i>ProductDAO</i> layer to execute the delete
     * on the table Product, it deletes  object c
     * @param c
     */
    public void delete(Product c) { abstractDAO.delete(c.getId());}
}
