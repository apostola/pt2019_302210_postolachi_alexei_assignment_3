package bll;

public interface Validator<Type> {
	public void validate(Type obj);
}
