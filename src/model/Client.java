package model;

/**
 * The object Client contains id, name information, address, mobile-phone number, email
 */
public class Client {
	private int id;
	private String firstName;
	private String lastName;
	private String address;
	private String phoneNumber;
	private String email;

	public Client() {

	}
	
	public Client(int id, String firstName, String lastName, String address, String phoneNumber, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	public Client(String firstName, String lastName, String address, String phoneNumber, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}
	
	//getters

	/**
	 * Getters
	 */
	public int getId() {return id;}
	public String getFirstName() {return firstName;}
	public String getLastName() {return lastName;}
	public String getAddress() {return address;}
	public String getPhoneNumber() {return phoneNumber;}
	public String getEmail() {return email;}
	
	//setters

	/**
	 *Setters
	 */
	public void setId(int id) {this.id = id;}
	public void setFirstName(String firstName) {this.firstName = firstName;}
	public void setLastName(String lastName) {this.lastName = lastName;}
	public void setAddress(String address) {this.address = address;}
	public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}
	public void setEmail(String email) {this.email = email;}
	
	//toString()
	public String toString() {
		return "Client [id= "+id+
				", First Name= "+firstName+
				", Last Name= "+lastName+
				", Email= "+email+
				", Phone= "+phoneNumber+
				", Address= "+address+
				"];";
	}
}
