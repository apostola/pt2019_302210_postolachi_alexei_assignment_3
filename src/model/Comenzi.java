package model;

/**
 * Comenzi objects have id of the object, client id, product id and quantity of the porduct that is ordered
 */
public class Comenzi {
	private int id;
	private int client;
	private int product;
	private int quantity;

	public Comenzi(){

	}
	
	public Comenzi(int id, int client, int product, int quantity) {
		this.id = id;
		this.client = client;
		this.product = product;
		this.quantity = quantity;
	}

	public Comenzi(int client, int product, int quantity) {
		this.client = client;
		this.product = product;
		this.quantity = quantity;
	}
	
	//getters
	/**
	 * Getters
	 */
	public int getId() {return id;}
	public int getClient() {return client;}
	public int getProduct() {return product;}
	public int getQuantity() {return quantity;}

	//setters
	/**
	 * Setters
	 */
	public void setId(int id) {this.id = id;}
	public void setClient(int client) {this.client = client;}
	public void setProduct(int product) {this.product = product;}
	public void setQuantity(int quantity) {this.quantity = quantity;}

	//toString()
	public String toString() {
		return "Client [id= "+id+
				", Client= "+client+
				", Product= "+product+
				"];";
	}
}
