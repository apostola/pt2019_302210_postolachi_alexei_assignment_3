package model;

/**
 * The product objects have the id of the product, name and info about manufacturer
 * weight, category, price, and the quantity of this kind of objects in the Ware-House
 */
public class Product {
	private int id;
	private String name;
	private String manufacturer;
	private int weight;
	private String category;
	private int price;
	private int quantity;

	public Product() {

	}

	public Product(int id, String name, String manufacturer, int weight, String category, int price, int quantity) {
		this.id = id;
		this.name = name;
		this.manufacturer = manufacturer;
		this.weight = weight;
		this.category = category;
		this.price = price;
		this.quantity = quantity;
	}

	public Product(String name, String manufacturer, int weight, String category, int price, int quantity) {
		this.id = id;
		this.name = name;
		this.manufacturer = manufacturer;
		this.weight = weight;
		this.category = category;
		this.price = price;
		this.quantity = quantity;
	}
	
	//getters
	/**
	 * Getters
	 */
	public int getId() {return id;}
	public String getName() {return name;}
	public String getManufacturer() {return manufacturer;}
	public int getWeight() {return weight;}
	public String getCategory() {return category;}
	public int getPrice() {return price;}
	public int getQuantity() {return quantity;}
	
	//setters
	/**
	 * Setters
	 */
	public void setId(int id) {this.id = id;}
	public void setName(String variable) {this.name = variable;}
	public void setManufacturer(String variable) {this.manufacturer = variable;}
	public void setWeight(int variable) {this.weight = variable;}
	public void setCategory(String variable) {this.category = variable;}
	public void setPrice(int variable) {this.price = variable;}
	public void setQuantity(int quantity) {this.quantity = quantity;}
	
	//toString()
	public String toString() {
		return "Client [id= "+id+
				", Name= "+name+
				", Category= "+category+
				", Manufacturer= "+ manufacturer +
				", Weight= "+weight+
				", Price= "+price+
				"];";
	}
}
