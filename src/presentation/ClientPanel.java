package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import model.Client;

public class ClientPanel {
	private JPanel mainPanel;
	private JPanel searchOperationPanel;
	private JPanel displayResultPanel;
	private JPanel inputPanel;

	private JTable resultText;
	private JTextField firstName;
	private JTextField lastName;
	private JTextField email;
	private JTextField phone;
	private JTextField address;

	private JTextField id;
	private JTextField findSurname;
	private JTextField findName;

	private JButton findButton;
	private JButton addButton;
	private JButton backButton;
	private JButton resetButton;
	private JButton updateButton;
	private JButton deletebutton;

	private DefaultTableModel dataModel;
	
	public ClientPanel() {
		initPanels();
		initComponents();
		setInputPanel();
		setSearchPanel();
		setDisplayResultPanel();
		setMainPanel();
	}
	
	protected void initPanels() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		searchOperationPanel = new JPanel();
		displayResultPanel = new JPanel();
		inputPanel = new JPanel();
	}
	
	protected void initComponents() {
		resultText = new JTable(10, 6);
		findButton = new JButton("Find");
		firstName = new JTextField();
		lastName = new JTextField();
		email = new JTextField();
		phone = new JTextField();
		address = new JTextField();
		id = new JTextField(10);
		addButton = new JButton("Add");
		backButton = new JButton("Back");
		resetButton = new JButton("Reset Table");
		updateButton = new JButton("Update");
		deletebutton = new JButton("Delete by ID");

		findSurname = new JTextField(10);
		findName = new JTextField(10);

		setTable();
	}
	
	protected void setInputPanel() {
		JPanel fNameP = new JPanel();
		fNameP.setLayout(new GridLayout(1, 2));
		JPanel lNameP = new JPanel();
		lNameP.setLayout(new GridLayout(1, 2));
		JPanel emailP = new JPanel();
		emailP.setLayout(new GridLayout(1, 2));
		JPanel addrP = new JPanel();
		addrP.setLayout(new GridLayout(1, 2));
		JPanel phoneP = new JPanel();
		phoneP.setLayout(new GridLayout(1, 2));
		
		fNameP.add(new JLabel("First Name:	"));
		fNameP.add(firstName);
		
		lNameP.add(new JLabel("Last name:	"));
		lNameP.add(lastName);
		
		emailP.add(new JLabel("Email:    "));
		emailP.add(email);
		
		addrP.add(new JLabel("Address:    "));
		addrP.add(address);
		
		phoneP.add(new JLabel("Phone nr:    "));
		phoneP.add(phone);
		
		inputPanel.setLayout(new GridLayout(6, 1));
		inputPanel.add(fNameP);
		inputPanel.add(lNameP);
		inputPanel.add(emailP);
		inputPanel.add(addrP);
		inputPanel.add(phoneP);
		inputPanel.add(addButton);
	}
	
	protected void setSearchPanel() {
		searchOperationPanel.add(new JLabel("Id:"));
		searchOperationPanel.add(id);
		searchOperationPanel.add(new JLabel("Nume:"));
		searchOperationPanel.add(findName);
		searchOperationPanel.add(new JLabel("Prenume:"));
		searchOperationPanel.add(findSurname);
		searchOperationPanel.add(findButton);
		searchOperationPanel.add(updateButton);
		searchOperationPanel.add(resetButton);
		searchOperationPanel.add(deletebutton);
	}
	
	protected void setDisplayResultPanel() {
		displayResultPanel.add(resultText);
	}
	
	protected void setMainPanel() {
		JPanel upperMenu = new JPanel();
		upperMenu.add(new JLabel("CLIENT MENU"));
		mainPanel.add(upperMenu);
		mainPanel.add(backButton);
		mainPanel.add(inputPanel);
		mainPanel.add(displayResultPanel);
		mainPanel.add(searchOperationPanel);
		mainPanel.add(backButton);
	}

	private void setTable() {
		String[] stringTip = {"id", "Nume", "Prenume", "email", "Telefon", "Adresa"};

		dataModel = new DefaultTableModel(null, stringTip);
		resultText.setModel(dataModel);
	}

	public void resetLogger() {
		dataModel.setRowCount(0);
	}

	public void addToLogger(Client c) {
		dataModel.addRow(new Object[] {String.valueOf(c.getId()), c.getFirstName(), c.getLastName(), c.getEmail(), c.getPhoneNumber(), c.getAddress()});
	}
	
	public void addFindListener(ActionListener e) {findButton.addActionListener(e);}
	public void addAddListener(ActionListener e) {addButton.addActionListener(e);}
	public void addBackListener(ActionListener e) {backButton.addActionListener(e);}
	public void addResetTableListener(ActionListener e) {resetButton.addActionListener(e);}
	public void addUpdateRowListener(ActionListener e) {updateButton.addActionListener(e);}
	public void addDeleteRowListener(ActionListener e) {deletebutton.addActionListener(e);}
	
	public JPanel getPanel() {
		return mainPanel;
	}

	public int getIdInput() {
		if (id.getText().length() == 0) {return 0;}
		return Integer.parseInt(id.getText());
	}

	public String getFindNameInput() {return findName.getText();}
	public String getFindSurnameInput() {return findSurname.getText();}
	public String getFirstName() {return firstName.getText();}
	public String getLastName() {return lastName.getText();}
	public String getMail() {return  email.getText();}
	public String getPhone() {return phone.getText();}
	public String getAddress() {return address.getText();}
}
