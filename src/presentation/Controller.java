package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import model.Client;
import model.Comenzi;
import model.Product;

import javax.swing.*;

public class Controller {
	View view;
	MenuPanel menuPanel = new MenuPanel();
	ClientPanel clientPanel = new ClientPanel();
	ProductPanel productPanel = new ProductPanel();
	OrderPanel orderPanel = new OrderPanel();

	ClientBLL clientBLL = new ClientBLL();
	ProductBLL productBLL = new ProductBLL();
	OrderBLL orderBLL = new OrderBLL();

	public Controller(View view) {
		this.view = view;
		view.setStartPage(menuPanel.getPanel());

		menuPanel.addClientListener(new addClientListener());
		menuPanel.addProductListener(new addProductListener());
		menuPanel.addOrderListener(new addOrderListener());

		clientPanel.addFindListener(new addClientFindListener());
		clientPanel.addAddListener(new addClientPutListener());
		clientPanel.addBackListener(new backCButtonListener());
		clientPanel.addResetTableListener(new addClientResetButtonListener());
		clientPanel.addUpdateRowListener(new addClientUpdateListener());
		clientPanel.addDeleteRowListener(new addClientDeleteButtonListener());

		productPanel.addFindListener(new addProductFindListener());
		productPanel.addAddListener(new addProductPutListener());
		productPanel.addBackListener(new backCButtonListener());
		productPanel.addResetTableListener(new addProductResetButtonListener());
		productPanel.addDeleteRowListener(new addProductDeleteButtonListener());

		orderPanel.addFindListener(new addOrderFindListener());
		orderPanel.addAddListener(new addOrderPutListener());
		orderPanel.addBackListener(new backCButtonListener());
		orderPanel.addResetTableListener(new addOrderResetButtonListener());
		orderPanel.addUpdateRowListener(new addOrderUpdateListener());
		orderPanel.addDeleteRowListener(new addOrderDeleteButtonListener());
	}

	public class backCButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.setStartPage(menuPanel.getPanel());
		}
	}

	public class addClientListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.setOtherPages(clientPanel.getPanel(), true);
		}
	}
	public class addProductListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.setOtherPages(productPanel.getPanel(), true);
		}
	}
	public class addOrderListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.setOtherPages(orderPanel.getPanel(), false);
		}
	}

	public class addClientFindListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clientPanel.resetLogger();
			int id = clientPanel.getIdInput();
			String name = clientPanel.getFindNameInput();
			String surname = clientPanel.getFindSurnameInput();
			if (id == 0 && (name.length() == 0) && (surname.length() == 0)) {
				ArrayList<Client> clienti = clientBLL.findAll();
				for (Client client : clienti) {
					clientPanel.addToLogger(client);
				}
			} else if (id != 0) {
				Client client = clientBLL.findClientById(id);
				clientPanel.addToLogger(client);
			} else {
				if (name.length() != 0) {
					ArrayList<Client> clienti = clientBLL.findAllBy("lastName", name);
					for (Client client : clienti) {
						clientPanel.addToLogger(client);
					}
				} else if (surname.length() != 0) {
					ArrayList<Client> clienti = clientBLL.findAllBy("firstName", surname);
					for (Client client : clienti) {
						clientPanel.addToLogger(client);
					}
				}
			}
		}
	}
	public class addClientPutListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clientPanel.resetLogger();

			String nume = clientPanel.getLastName();
			String prenume = clientPanel.getFirstName();
			String phone = clientPanel.getPhone();
			String email = clientPanel.getMail();
			String address = clientPanel.getAddress();
			Client c = new Client(prenume, nume, address, phone, email);

			clientBLL.insert(c);

			ArrayList<Client> clienti = clientBLL.findAll();
			for (Client client : clienti) {
				clientPanel.addToLogger(client);
			}
		}
	}
	public class addClientResetButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clientPanel.resetLogger();
		}
	}
	public class addClientUpdateListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clientPanel.resetLogger();

			int id = clientPanel.getIdInput();
			String nume = clientPanel.getLastName();
			String prenume = clientPanel.getFirstName();
			String phone = clientPanel.getPhone();
			String email = clientPanel.getMail();
			String address = clientPanel.getAddress();
			Client c = new Client(id, prenume, nume, address, phone, email);

			clientBLL.update(c);
			Client client = clientBLL.findClientById(id);
			clientPanel.addToLogger(client);
		}
	}
	public class addClientDeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clientPanel.resetLogger();

			int orderId = clientPanel.getIdInput();
			Client client = clientBLL.findClientById(orderId);

			clientBLL.delete(client);
		}
	}

	public class addProductFindListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			productPanel.resetLogger();
			int id = Integer.valueOf(productPanel.getId());
			String name = productPanel.getInputName();
			String manufacturer = productPanel.getInputManufacturer();
			if (id == 0 && (name.length() == 0) && (manufacturer.length() == 0)) {
				ArrayList<Product> products = productBLL.findAll();
				for (Product product : products) {
					productPanel.addToLogger(product);
				}
			} else if (id != 0) {
				Product product = productBLL.findProductById(id);
				productPanel.addToLogger(product);
			} else {
				if (name.length() != 0) {
					ArrayList<Product> products = productBLL.findAllBy("name", name);
					for (Product prodcut : products) {
						productPanel.addToLogger(prodcut);
					}
				} else if (manufacturer.length() != 0) {
					ArrayList<Product> products = productBLL.findAllBy("manufacturer", manufacturer);
					for (Product product : products) {
						productPanel.addToLogger(product);
					}
				}
			}
		}
	}
	public class addProductPutListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			productPanel.resetLogger();

			String nume = productPanel.getName();
			String manufacturer = productPanel.getManufacturer();
			int weight = Integer.parseInt(productPanel.getWeights());
			String category = productPanel.getCategory();
			int price = Integer.parseInt(productPanel.getPrice());
			int quantity = Integer.parseInt(productPanel.getQunatity());
			Product product = new Product(nume, manufacturer, weight, category, price, quantity);

			productBLL.insert(product);

			ArrayList<Product> clienti = productBLL.findAll();
			for (Product products : clienti) {
				productPanel.addToLogger(products);
			}
		}
	}
	public class addProductResetButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			productPanel.resetLogger();
		}
	}
	public class addProductDeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			productPanel.resetLogger();

			int orderId = productPanel.getId();
			Product product = productBLL.findProductById(orderId);

			productBLL.delete(product);
		}
	}

	public class addOrderFindListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			orderPanel.resetLogger();

			int idClient = orderPanel.getClientId();
			int idProduct = orderPanel.getProducttId();
			int idOrder = orderPanel.getId();

			if (idClient == 0) {
				ArrayList<Client> clienti = clientBLL.findAll();
				for (Client client : clienti) {
					orderPanel.addToClientLogger(client);
				}
			} else {
				Client client = clientBLL.findClientById(idClient);
				orderPanel.addToClientLogger(client);
			}

			if (idProduct == 0) {
				ArrayList<Product> products = productBLL.findAll();
				for (Product product : products) {
					orderPanel.addToPoductLogger(product);
				}
			} else {
				Product product = productBLL.findProductById(idClient);
				orderPanel.addToPoductLogger(product);
			}

			if (idOrder == 0) {
				ArrayList<Comenzi> comenzis = orderBLL.findAll();
				for (Comenzi comenzi : comenzis) {
					orderPanel.addToOrderLogger(comenzi);
				}
			} else {
				Comenzi comenzi = orderBLL.findOrderById(idClient);
				orderPanel.addToOrderLogger(comenzi);
			}
		}
	}
	public class addOrderPutListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			orderPanel.resetLogger();

			int idClient = orderPanel.getClientId();
			int idProduct = orderPanel.getProducttId();
			int quantity = orderPanel.getQuantity();
			Comenzi c = new Comenzi(idClient, idProduct, quantity);


			Product product = productBLL.findProductById(idProduct);

			if (product.getQuantity() < quantity) {
				JOptionPane.showMessageDialog(productPanel.getPanel(), "Numarul de produse in stock este mai mica decat cantitatea dorita!");
			} else {
				orderBLL.insert(c);
				product.setQuantity(product.getQuantity() - quantity);
				productBLL.update(product);

				ArrayList<Comenzi> comenzis = orderBLL.findAll();
				for (Comenzi comenzi : comenzis) {
					orderPanel.addToOrderLogger(comenzi);
				}

				Document document = new Document();
				try
				{
					PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("cont_" + c.getClient()+c.getProduct() + ".pdf"));
					document.open();
					document.add(new Paragraph(" Client id: " +  c.getClient() +
							" Client nume: "+ clientBLL.findClientById(c.getClient()).getFirstName() + " Produs: " + c.getProduct() +
							" Pret: " + c.getQuantity() * productBLL.findProductById(c.getProduct()).getPrice()) );
					document.close();
					writer.close();
				} catch (DocumentException exception){
					exception.printStackTrace();
				} catch (FileNotFoundException eexception2){
					eexception2.printStackTrace();
				}
			}
		}
	}
	public class addOrderResetButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
				orderPanel.resetLogger();
			}
	}
	public class addOrderUpdateListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			orderPanel.resetLogger();

			int orderId = orderPanel.getId();
			int clientId = orderPanel.getClientId();
			int productId = orderPanel.getProducttId();
			int quantity = orderPanel.getQuantity();
			Comenzi comenzi = new Comenzi(orderId, clientId, productId, quantity);

			orderBLL.update(comenzi);
			Comenzi orderf = orderBLL.findOrderById(orderId);
			orderPanel.addToOrderLogger(orderf);
		}
	}
	public class addOrderDeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			orderPanel.resetLogger();

			int orderId = orderPanel.getId();
			Comenzi order = orderBLL.findOrderById(orderId);

			orderBLL.delete(order);
		}
	}
}

