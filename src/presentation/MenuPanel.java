package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MenuPanel {
	JPanel startMenu;
	JButton clientMenuButton;
	JButton orderMenuButton;
	JButton productMenuButton;
	
	public MenuPanel() {
		startMenu = new JPanel();
		startMenu.setLayout(new GridLayout(4, 1));
		clientMenuButton = new JButton("Client");
		orderMenuButton = new JButton("Comanda");
		productMenuButton = new JButton("Produs");
		
		startMenu.add(new JLabel("Alegeti categoria cu care doriti sa faceti operatii:"));
		startMenu.add(clientMenuButton);
		startMenu.add(orderMenuButton);
		startMenu.add(productMenuButton);
	}
	
	public void addClientListener(ActionListener e) {clientMenuButton.addActionListener(e);}
	public void addOrderListener(ActionListener e) {orderMenuButton.addActionListener(e);}
	public void addProductListener(ActionListener e) {productMenuButton.addActionListener(e);}
	
	public JPanel getPanel() {return startMenu;}
}
