package presentation;


import model.Client;
import model.Comenzi;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;

public class OrderPanel {
    private JPanel mainPanel;
    private JPanel upperPanel;
    private JPanel clientPanel;
    private JPanel productPanel;
    private JPanel inputPanel;
    private JPanel buttonsPanel;
    private JPanel orderPanel;

    private JTable clientTable;
    private JTable productsTable;
    private JTable orders;

    private DefaultTableModel defaultTableClient;
    private DefaultTableModel defaultTableProduct;
    private DefaultTableModel defaultTableOrder;

    private JTextField clientId;
    private JTextField productId;
    private JTextField orderId;
    private JTextField qunatity;

    private JButton placeOrder;
    private JButton find;
    private JButton update;
    private JButton reset;
    private JButton back;
    private JButton deletebutton;

    private JScrollPane clientScrollPane;
    private JScrollPane productScrollPane;
    private JScrollPane orderScrollPane;

    public OrderPanel() {
        initPanels();
        initComponents();
        setTables();
        setComponents();
    }

    protected void initPanels() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        upperPanel = new JPanel();
        mainPanel.add(upperPanel);

        clientPanel = new JPanel();
        productPanel = new JPanel();
        upperPanel.add(clientPanel);
        upperPanel.add(productPanel);

        inputPanel = new JPanel();
        buttonsPanel = new JPanel();
        orderPanel = new JPanel();
    }

    protected void initComponents() {
        clientTable = new JTable(5, 3);
        productsTable = new JTable(5, 3);
        orders = new JTable(10, 3);

        clientId = new JTextField(10);
        productId = new JTextField(10);
        orderId = new JTextField(10);
        qunatity = new JTextField(10);

        placeOrder = new JButton("Place order");
        find = new JButton("Find");
        update = new JButton("Update");
        reset = new JButton("Reset Table");
        back = new JButton("Back to Menu");
        deletebutton = new JButton("Delete by ID");

        clientScrollPane = new JScrollPane();
        productScrollPane = new JScrollPane();
        orderScrollPane = new JScrollPane();
    }

    protected void setComponents() {
        clientScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        productScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        orderScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);


//        JScrollPane scrollPane = new JScrollPane();
        clientScrollPane.setViewportView(clientTable);
//        clientScrollPane.setSize(300, 200);

        productScrollPane.setViewportView(productsTable);
//        productScrollPane.setSize(300, 200);

        orderScrollPane.setViewportView(orders);
//        orderScrollPane.setSize(300, 200);

//        clientScrollPane.add(clientTable);
//        orderScrollPane.add(orders);
//        productScrollPane.add(productsTable);
//        clientPanel.add(scrollPane);

        clientPanel.add(clientScrollPane);
        productPanel.add(productScrollPane);
        orderPanel.add(orderScrollPane);

        inputPanel.add(new JLabel("id:"));
        inputPanel.add(orderId);
        inputPanel.add(new JLabel("ClientId:"));
        inputPanel.add(clientId);
        inputPanel.add(new JLabel("ProductId:"));
        inputPanel.add(productId);
        inputPanel.add(new JLabel("Quantity:"));
        inputPanel.add(qunatity);
        inputPanel.add(find);
        inputPanel.add(reset);

        buttonsPanel.add(placeOrder);
        buttonsPanel.add(update);
        buttonsPanel.add(deletebutton);

        mainPanel.add(upperPanel);
        mainPanel.add(inputPanel);
        mainPanel.add(buttonsPanel);
        mainPanel.add(orderPanel);
        mainPanel.add(back);
    }

    private void setTables() {
        String[] stringTipClient = {"id", "Nume", "Prenume"};
        String[] stringTipProduct = {"id", "Nume", "Prducator", "stock"};
        String[] stringTipOrder = {"id", "idClient", "idProducator", "Cantitate"};

        defaultTableClient = new DefaultTableModel(null, stringTipClient);
        defaultTableClient.setColumnIdentifiers(new Object[] {"id" , "Nume", "Prenume"});

        defaultTableProduct = new DefaultTableModel(null, stringTipProduct);
        defaultTableProduct.setColumnIdentifiers(new Object[] {"id" , "Nume", "Producator", "Stock nr."});

        defaultTableOrder = new DefaultTableModel(null, stringTipOrder);
        defaultTableOrder.setColumnIdentifiers(new Object[] {"id" , "idClient", "idProduct", "cantitate"});

        clientTable.setModel(defaultTableClient);
        productsTable.setModel(defaultTableProduct);
        orders.setModel(defaultTableOrder);
    }

    public void resetLogger() {
        defaultTableClient.setRowCount(0);
        defaultTableProduct.setRowCount(0);
        defaultTableOrder.setRowCount(0);
    }

    public void addToPoductLogger(Product c) {
        defaultTableProduct.addRow(new Object[] {String.valueOf(c.getId()), c.getName(), c.getManufacturer(), c.getQuantity()});
    }

    public void addToClientLogger(Client c) {
        defaultTableClient.addRow(new Object[] {String.valueOf(c.getId()), c.getLastName(), c.getFirstName()});
    }

    public void addToOrderLogger(Comenzi c) {
        defaultTableOrder.addRow(new Object[] {String.valueOf(c.getId()), String.valueOf(c.getClient()), String.valueOf(c.getProduct()), String.valueOf(c.getQuantity())});
    }

    public JPanel getPanel() {
        return mainPanel;
    }

    public int getId() {
        try {
            return Integer.parseInt(orderId.getText());
        } catch(Exception e) {
            if (orderId.getText().length() != 0)
                JOptionPane.showMessageDialog(mainPanel, "Id comanda incorect");
        }
        return 0;
    }

    public int getClientId() {
        try {
            return Integer.parseInt(clientId.getText());
        } catch(Exception e) {
            if (clientId.getText().length() != 0)
                JOptionPane.showMessageDialog(mainPanel, "Id client incorect");
        }
        return 0;
    }

    public int getProducttId() {
        try {
            return Integer.parseInt(productId.getText());
        } catch(Exception e) {
            if (productId.getText().length() != 0)
                JOptionPane.showMessageDialog(mainPanel, "Id product incorect");
        }
        return 0;
    }

    public int getQuantity() {
        try {
            return Integer.parseInt(qunatity.getText());
        } catch(Exception e) {
            if (qunatity.getText().length() != 0)
                JOptionPane.showMessageDialog(mainPanel, "Id product incorect");
        }
        return 0;
    }

    public void addFindListener(ActionListener e) {find.addActionListener(e);}
    public void addAddListener(ActionListener e) {placeOrder.addActionListener(e);}
    public void addBackListener(ActionListener e) {back.addActionListener(e);}
    public void addResetTableListener(ActionListener e) {reset.addActionListener(e);}
    public void addUpdateRowListener(ActionListener e) {update.addActionListener(e);}
    public void addDeleteRowListener(ActionListener e) {deletebutton.addActionListener(e);}

}
