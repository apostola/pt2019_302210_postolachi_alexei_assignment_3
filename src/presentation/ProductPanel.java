package presentation;

import model.Product;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ProductPanel {
	private JPanel mainPanel;
	private JPanel searchOperationPanel;
	private JPanel displayResultPanel;
	private JPanel inputPanel;

	private JTable resultText;
	private JTextField id;
	private JTextField name;
	private JTextField manufacturer;
	private JTextField weight;
	private JTextField category;
	private JTextField price;
	private JTextField quantity;
	private JTextField inputName;
	private JTextField inputManufacturer;

	private JButton findButton;
	private JButton addButton;
	private JButton backButton;
	private JButton resetButton;
	private JButton updateButton;
	private JButton deletebutton;

	private DefaultTableModel dataModel;
	
	public ProductPanel() {
		initPanels();
		initComponents();
		setInputPanel();
		setSearchPanel();
		setDisplayResultPanel();
		setMainPanel();
		setTable();
	}
	
	protected void initPanels() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		searchOperationPanel = new JPanel();
		displayResultPanel = new JPanel();
		inputPanel = new JPanel();
	}
	
	protected void initComponents() {
		resultText = new JTable(10, 6);
		findButton = new JButton("Find");
		name = new JTextField();
		manufacturer = new JTextField();
		weight = new JTextField();
		category = new JTextField();
		price = new JTextField();
		quantity = new JTextField();
		id = new JTextField(10);
		addButton = new JButton("Add");
		backButton = new JButton("Back");
		resetButton = new JButton("Reset Table");
		updateButton = new JButton("Update");
		deletebutton = new JButton("Delete by ID");

		inputName = new JTextField(10);
		inputManufacturer = new JTextField(10);
	}
	
	protected void setInputPanel() {
		JPanel nameP = new JPanel();
		nameP.setLayout(new GridLayout(1, 2));
		JPanel manufacturerP = new JPanel();
		manufacturerP.setLayout(new GridLayout(1, 2));
		JPanel wightP = new JPanel();
		wightP.setLayout(new GridLayout(1, 2));
		JPanel categoryP = new JPanel();
		categoryP.setLayout(new GridLayout(1, 2));
		JPanel priceP = new JPanel();
		priceP.setLayout(new GridLayout(1, 2));
		JPanel qunatityP = new JPanel();
		qunatityP.setLayout(new GridLayout(1, 2));

		nameP.add(new JLabel("Name:"));
		nameP.add(name);
		
		manufacturerP.add(new JLabel("Manufacturer:"));
		manufacturerP.add(manufacturer);
		
		wightP.add(new JLabel("Weight:    "));
		wightP.add(weight);
		
		categoryP.add(new JLabel("Category:"));
		categoryP.add(category);
		
		priceP.add(new JLabel("Price($):"));
		priceP.add(price);

		qunatityP.add(new JLabel("Quantity:"));
		qunatityP.add(quantity);
		
		inputPanel.setLayout(new GridLayout(7, 1));
		inputPanel.add(nameP);
		inputPanel.add(manufacturerP);
		inputPanel.add(wightP);
		inputPanel.add(categoryP);
		inputPanel.add(priceP);
		inputPanel.add(qunatityP);
		inputPanel.add(addButton);
	}

	private void setTable() {
		String[] stringTip = {"id", "Nume", "Producator", "Weight", "Category", "Price", "Quantity"};

		dataModel = new DefaultTableModel(null, stringTip);
		resultText.setModel(dataModel);
	}
	
	protected void setSearchPanel() {
		searchOperationPanel.add(new JLabel("EnterID: "));
		searchOperationPanel.add(id);
		searchOperationPanel.add(new JLabel("Name: "));
		searchOperationPanel.add(inputName);
		searchOperationPanel.add(new JLabel("Manufacturer: "));
		searchOperationPanel.add(inputManufacturer);
		searchOperationPanel.add(findButton);
		searchOperationPanel.add(updateButton);
		searchOperationPanel.add(resetButton);
		searchOperationPanel.add(deletebutton);
	}

	protected void setDisplayResultPanel() {
		displayResultPanel.add(resultText);
	}
	
	protected void setMainPanel() {
		JPanel upperMenu = new JPanel();
		upperMenu.add(new JLabel("PRODUCT MENU"));
		mainPanel.add(upperMenu);
		mainPanel.add(inputPanel);
		mainPanel.add(displayResultPanel);
		mainPanel.add(searchOperationPanel);
		mainPanel.add(backButton);
	}

	public void resetLogger() {
		dataModel.setRowCount(0);
	}

	public void addToLogger(Product c) {
		dataModel.addRow(new Object[] {String.valueOf(c.getId()), c.getName(), c.getManufacturer(), c.getWeight(), c.getCategory(), c.getPrice(), c.getQuantity()});
	}
	
	public void addFindListener(ActionListener e) {findButton.addActionListener(e);}
	public void addAddListener(ActionListener e) {addButton.addActionListener(e);}
	public void addBackListener(ActionListener e) {backButton.addActionListener(e);}
	public void addResetTableListener(ActionListener e) {resetButton.addActionListener(e);}
	public void addUpdateRowListener(ActionListener e) {updateButton.addActionListener(e);}
	public void addDeleteRowListener(ActionListener e) {deletebutton.addActionListener(e);}
	
	public JPanel getPanel() {
		return mainPanel;
	}
	public int getId() {
		if (id.getText().length() == 0) {return 0;}
		return Integer.parseInt(id.getText());
	}
	public String getName() {return name.getText();}
	public String getManufacturer() {return manufacturer.getText();}
	public String getWeights() {return weight.getText();}
	public String getCategory() {return category.getText();}
	public String getPrice() {return price.getText();}
	public String getInputName() {return inputName.getText();}
	public String getInputManufacturer() {return inputManufacturer.getText();}
	public String getQunatity() {return quantity.getText();}
}
