package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class View extends JFrame{
	JPanel window;
	
	public View() {
		window = new JPanel();
		
		this.setContentPane(window);
		this.setTitle("Administration Panel");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void setStartPage(JPanel panel) {
		window.removeAll();
		window.add(panel);
		
		this.setSize(300, 180);
		this.setVisible(true);
	}
	
	public void setOtherPages(JPanel panel, boolean i) {
		window.removeAll();
		window.add(panel);

		if (i)
			this.setSize(840, 480);
		else this.setSize(1000, 600);
		this.setVisible(true);
	}
}
