package start;

import presentation.*;;

public class Simulate {

	public static void main(String[] args) {
		View view = new View();
		@SuppressWarnings("unused")
		Controller controller = new Controller(view);
	}
}
